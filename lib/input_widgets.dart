import 'package:flutter/material.dart';

class InputWidgets extends StatefulWidget {
  @override
  State<InputWidgets> createState() => _InputWidgetsState();
}

class _InputWidgetsState extends State<InputWidgets> {
  final myController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            color: Colors.white,
            child: TextField(
              controller: myController,
              onChanged: (value) {
                print('OnCHANGE:::$value');
              },
              onEditingComplete: () {
                print('ONCOMPLETE:::${myController.text.toString()}');
              },
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Type Anything',
                filled: true,
                fillColor: Colors.blue,
                contentPadding:
                    const EdgeInsets.only(left: 14.0, bottom: 6.0, top: 6.0),
                focusedBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: Colors.red,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: Colors.yellow,
                    width: 10,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
